import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { InboxOutlined } from '@material-ui/icons'
import React from 'react';



const MenuItem = ({ menu }) => {

    return (
        <ListItem button component="a" href={menu.url}>
            <ListItemIcon>
                {<menu.icon />}
            </ListItemIcon>
            <ListItemText primary={menu.titulo}> </ListItemText>
        </ListItem>
    )
}

export default MenuItem
