import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Header from './../Header';

const useStyles = makeStyles({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
});

export default function TemporaryDrawer({ children, titulo }) {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    return (
        <>

            <Header
                handleMenu={() => setIsOpen(!isOpen)}
            />


            <Drawer
                open={isOpen}
                onClose={()=>setIsOpen(false)}
            >
                {children}
            </Drawer>
        </>
    );
}