import { List } from '@material-ui/core';
import React from 'react'
import MenuItem from './MenuItem';
import Drawer from './Drawer'

import { ContactsRounded, HomeRounded, ShoppingBasketRounded } from '@material-ui/icons';
const index = () => {

    const menuArr = [
        {
            id: 1,
            titulo: "inicio",
            url: '/',
            icon: HomeRounded,
        },
        {
            id: 2,
            titulo: "productos",
            url: '/productos',
            icon: ShoppingBasketRounded
        },
        {
            id: 3,
            titulo: "servicios",
            url: '/servicios',
            icon: ContactsRounded
        },

    ];


    return (
        <Drawer>
            <List>
                {
                    menuArr.map(item => <MenuItem key={item.id} menu={item} />)
                }
            </List>
        </Drawer>
    )
}

export default index
