import React, { useState } from 'react';
import { TextField, Button, CircularProgress, Grid, InputAdornment, IconButton, Input } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Visibility, VisibilityOff } from '@material-ui/icons';

const useStyles = makeStyles({
    contenedor: {
        display: 'flex',
        flexDirection: 'column'
    },
    input: {
        padding: '.3em'
    },

    formulario: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth: '30em'
    }
});


const LoginForm = ({ login }) => {


    const [showPass, setShowPass] = useState(false);

    const iValues = {
        user: '',
        password: ''
    };

    const onSubmit = async (values, { setErrors, setStatus, setSubmitting }) => {
        try {
            login(values);
        } catch (e) {

        }
    };

    const validationSchema = Yup.object().shape({
        user: Yup.string().min(4, 'usuario muy corto').required('Este campo es obligatorio'),
        password: Yup.string().min(4, 'clave no valida').required('Este campo es obligatorio'),
    });


    const classes = useStyles();


    return (

        <Formik
            initialValues={iValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
        >
            {
                ({
                    errors,
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    isSubmitting,
                    setFieldValue,
                    touched,
                    values }) =>
                    <form onSubmit={handleSubmit} className={classes.formulario}>

                        <h1>{showPass ? 'verdadero' : 'falso'}</h1>

                        <TextField
                            label="Usuario"
                            type="text"
                            variant="filled"
                            className={classes.input}
                            name="user"
                            value={values.user}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.user && errors.user}
                            error={touched.user && errors.user}
                        />

                        <Input
                            label="Contrase;a"
                            type={showPass ? 'text' : 'password'}
                            variant="filled"
                            className={classes.input}
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.password && errors.password}
                            error={touched.password && errors.password}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={() => setShowPass(!showPass)}
                                    >
                                        {showPass ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />



                        <Button type="submit" disabled={isSubmitting} >
                            {
                                isSubmitting ? <CircularProgress size={30} /> : 'Guardar'
                            }
                        </Button>

                    </form>


            }
        </Formik>

    )
}

export default LoginForm;
