import React from 'react';
import JSONTree from 'react-json-tree';
import useAuth from '../../contextApi/hooks/useAuth';
import LoginForm from './LoginForm';


const Login = () => {



  const { login, logout, isAuthenticated, user } = useAuth();
  


  return (
    <>
      <JSONTree data={user} />
      <LoginForm login={login} />
    </>
  )
}

export default Login;
