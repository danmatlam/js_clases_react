import React, { useState } from 'react';
import { TextField, Button, CircularProgress, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Formik } from 'formik';
import * as Yup from 'yup';

const useStyles = makeStyles({
    contenedor: {
        display: 'flex',
        flexDirection: 'column'
    },
    input: {
        padding: '.3em'
    },

    formulario: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth:'30em'
    }
});


const ProductoForm = ({ producto, submitProducto }) => {



    const iValues = (producto && producto.id)
        ? {
            id: producto.id,
            nombre: producto.nombre,
            descripcion: producto.descripcion,
            precio: producto.precio
        }
        : {
            id: '',
            nombre: '',
            descripcion: '',
            precio: '',
        };


    const onSubmit = async (values, { setErrors, setStatus, setSubmitting }) => {
        try {
            submitProducto(values);
        } catch (e) {

        }
    };


    const validationSchema = Yup.object().shape({
        nombre: Yup.string().min(3, 'Nombre producto muy corto').max(12, 'Nombre producto muy largo').required('Este campo es obligatorio'),
        descripcion: Yup.string().min(20, 'Descripcion del producto muy corta').required('Este campo es obligatorio'),
        precio: Yup.number().required()
    });




    const classes = useStyles();


    return (
        <Grid item xs={12} sm={6} md={4}>
            <h2>Producto Formulario</h2>
            <Formik
                initialValues={iValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
            >
                {
                    ({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting,
                        setFieldValue,
                        touched,
                        values }) =>
                        <form onSubmit={handleSubmit} className={classes.formulario}>

                            <TextField
                                label="Nombre"
                                type="text"
                                variant="filled"
                                className={classes.input}

                                name="nombre"
                                value={values.nombre}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={touched.nombre && errors.nombre}
                                error={touched.nombre && errors.nombre}


                            />


                            <TextField
                                label="Descripcion"
                                type="text"
                                variant="filled"
                                className={classes.input}
                                value={values.descripcion}
                                onChange={handleChange}
                                onBlur={handleBlur}

                                name="descripcion"
                                helperText={touched.descripcion && errors.descripcion}
                                error={touched.descripcion && errors.descripcion}

                                multiline
                                rows={4}


                            />


                            <TextField
                                label="Precio"
                                type="number"
                                variant="filled"
                                className={classes.input}
                                value={values.precio}
                                onChange={handleChange}
                                onBlur={handleBlur}

                                name="precio"
                                helperText={touched.precio && errors.precio}
                                error={touched.precio && errors.precio}

                            />

                            <Button type="submit" disabled={isSubmitting} >
                                {
                                    isSubmitting ? <CircularProgress size={30} /> : 'Guardar'
                                }
                            </Button>

                        </form>


                }
            </Formik>
        </Grid>
    )
}

export default ProductoForm;
