import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardActionArea, CardActions, CardContent, CardMedia, Button, Typography } from '@material-ui/core'
import ProductoForm from './ProductoForm';


const useStyles = makeStyles({
    cartita: {
        margin: 12,
        borderRadius: 9,
    },
    imagen: {
        height: 200,
    },
});

const Producto = ({ id, nombre, descripcion, precio, fotoUrl, submitProducto, submitDeleteProducto }) => {
    const [editable, setEditable] = useState(false);
    const classes = useStyles();

    const producto = {
        id,
        nombre,
        descripcion,
        precio
    }


    return (
        <Grid item xs={12} sm={6} md={4}>
            {
                !editable ?
                    /// si editable es falso muestra producto
                    <Card className={classes.cartita}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.imagen}
                                image={fotoUrl}
                                title={nombre}
                            />

                            <CardContent>

                                <Grid container alignItems="center">
                                    <Grid item md={9}>
                                        <Typography gutterBottom variant="h4" component="h2">
                                            {nombre}
                                        </Typography>
                                    </Grid>
                                    <Grid item md={3}>
                                        <Typography gutterBottom variant="h6" component="h2">
                                            ${precio}
                                        </Typography>
                                    </Grid>
                                </Grid>

                                <Typography variant="body1" color="textSecondary" component="p">
                                    {descripcion}
                                </Typography>

                            </CardContent>

                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary" onClick={() => setEditable(true)}>
                                Editar
                            </Button>
                            <Button size="small" color="primary" onClick={() => submitDeleteProducto(id)}>
                                Eliminar
                            </Button>
                            <Button size="small" color="primary">
                                Comprar
                            </Button>
                        </CardActions>
                    </Card>
                    /// caso contrrario muestr formulario
                    : <ProductoForm

                        producto={producto}
                        submitProducto={submitProducto}
                    />
            }
        </Grid>
    )
}

Producto.propTypes = {
    nombre: PropTypes.string.isRequired,
    descripcion: PropTypes.string.isRequired,
    precio: PropTypes.string.isRequired,
}

export default Producto;
