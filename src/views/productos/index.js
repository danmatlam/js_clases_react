import React, { useState, useEffect } from 'react'
import Producto from './Producto';
import { CircularProgress, Grid } from '@material-ui/core';
import { getProductos, getProducto, updateProducto, deleteProducto, createProducto } from '../../slices/producto'
import { useDispatch, useSelector } from '../../store';
import Header from '../../components/Header';
import ProductoForm from './ProductoForm';


const ProductosContainer = () => {
    const dispatch = useDispatch();

    const [esCrear, setEsCrear] = useState(false);

    useEffect(
        () => {
            dispatch(getProductos());
            // dispatch(getProducto(3));
        },
        []
    );

    const productoState = useSelector((state) => state.producto);
    const { productos, producto, loading, error } = productoState;


    const _deleteProducto = (id) => {
        dispatch(deleteProducto(id));
    }
    const _createProducto = (payload) => {
        dispatch(createProducto(payload))
    }
    const _updateProducto = (payload) => {
        dispatch(updateProducto(payload))
    }






    return (
        <div>
            {/* <Header title="Productos" onCreate={() => { setEsCrear(!esCrear) }} /> */}
            {
                (loading) 
                ? <CircularProgress />
                : <Grid container direction="row">
                        {
                            esCrear &&
                            <ProductoForm
                                producto={null}
                                submitProducto={_createProducto}
                            />
                        }
                        {
                            productos.map(item =>
                                <Producto
                                    key={item.id}
                                    {...producto}
                                   
                                   
                                    id={item.id}
                                    nombre={item.nombre}
                                    descripcion={item.descripcion}
                                    precio={item.precio}
                                    fotoUrl={item.fotoUrl}


                                    submitProducto={_updateProducto}
                                    submitDeleteProducto={_deleteProducto}
                                />)
                        }
                    </Grid>
            }


        </div>
    )
}

export default ProductosContainer;
