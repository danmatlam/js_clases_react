import React, {useEffect} from 'react'
import JSONTree from 'react-json-tree';
import { useParams } from 'react-router-dom';
import { getProducto, getProductos } from '../../slices/producto';
import { useDispatch, useSelector } from '../../store';

const ProductoId = () => {
    const {id} = useParams();
    const dispatch = useDispatch();

    useEffect(
        () => {
            dispatch(getProducto(id))
        },[]
    );

    const productoState = useSelector((state) => state.producto);
    const { productos, producto, loading, error } = productoState;

    return (
        <div>
                <JSONTree data={productoState}/>

                
        </div>
    )
}

export default ProductoId
