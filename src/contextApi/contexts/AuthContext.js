import React, { createContext, useEffect, useReducer } from 'react';
import jwtDecode from 'jwt-decode';
import axios from 'axios';

const baseurl = "http://localhost:1337";

const initialState = {
  isAuthenticated: false,
  isInitialised: false,
  user: null,
  error: false
};


const isValidToken = (accessToken) => {
  if (!accessToken) return false
  const decoded = jwtDecode(accessToken);
  return decoded.exp > (Date.now() / 1000);
};


const setSession = (accessToken) => {
  if (accessToken) {
    localStorage.setItem('accessToken', accessToken);
    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
  } else {
    localStorage.removeItem('accessToken');
    delete axios.defaults.headers.common.Authorization;
  }
};


const authReducer = (state, action) => {
  switch (action.type) {
    case 'INITIALISE': {
      const { isAuthenticated, user } = action.payload;
      return {
        ...state,
        isAuthenticated,
        isInitialised: true,
        user
      };
    }
    case 'LOGIN_OK': {
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user
      };
    }
    case 'CUANDO_ERROR': {
      return {
        ...state,
        isAuthenticated: false,
        user: action.payload.user,
        error: action.payload.error
      };
    }
    default: {
      return { ...state };
    }
  }
};




const AuthContext = createContext({
  ...initialState,
  method: 'JWT',
  login: () => Promise.resolve(),
  logout: () => { }
});


export default AuthContext;






export const AuthProvider = ({ children }) => {

  const [state, dispatch] = useReducer(authReducer, initialState);


  const login = async (payload) => {

    const {user, password}= payload

    debugger

    const res = await axios.post(baseurl + '/auth/local', { "identifier": user, "password": password });
    
    debugger

    if (res && res.data && res.data.jwt) {
      setSession(res.data.jwt);
    }


    // ALIMENTO EL
    if (!res.data || res.error) {
      dispatch({
        type: 'CUANDO_ERROR',
        payload: {
          user: null,
          error: res.error ? res.error : true
        }
      });
    }
    else {
      debugger;
      dispatch({
        type: 'LOGIN_OK',
        payload: {
          user: res.data.user,
          error: false
        }
      });
    }




  };

  const initialise = async () => {
    try {
      const accessToken = window.localStorage.getItem('accessToken');

      debugger;

      if (accessToken && isValidToken(accessToken)) {
        setSession(accessToken);
        const config = {
          headers: { Authorization: `Bearer ${accessToken}` }
        }
        const response = await axios.get(baseurl + '/users/me', config);
        const user = response.data.user;
        dispatch({
          type: 'INITIALISE',
          payload: { isAuthenticated: true, user }
        });
      } else {
        dispatch({
          type: 'INITIALISE',
          payload: {
            isAuthenticated: false,
            user: null
          }
        });
      }
    } catch (err) {
      console.error(err);
      dispatch({
        type: 'INITIALISE',
        payload: { isAuthenticated: false, user: null }
      });
    }
  };



  const logout = () => {
    setSession(null);
    dispatch({ type: 'LOGOUT' });
  };






  /// USE EFFECT PARA EJECUTAR VALIDACION EN BASE A TOKENS
  useEffect(() => {
    initialise();
  }, []);



  return (
    <AuthContext.Provider value={{ ...state, method: 'JWT', login, logout }} >
      {children}
    </AuthContext.Provider>
  );
};