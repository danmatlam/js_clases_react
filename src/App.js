import React from 'react'
import ProductosContainer from './views/productos';
import { Provider as ReduxProvider } from 'react-redux';
import store from './store'
import ProductoId from './views/productos/ProductoId';
import MenuContainer from './components/menu'
import Drawer from './components/menu/Drawer'
import Login from './views/login'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { AuthProvider } from './contextApi/contexts/AuthContext';


const App = () => {
  return (
    <AuthProvider>
      <ReduxProvider store={store}>
        <Router>

          <MenuContainer />


          <Switch>
            <Route exact path="/">
              <h1>Esto es el home</h1>
            </Route>

            <Route exact path="/login">
              <Login></Login>
            </Route>

            <Route exact path="/productos">
              <ProductosContainer />
            </Route>
            <Route exact path="/productos/:id">
              <ProductoId />
            </Route>
            <Route path="/servicios">
              <h1>Esto es servicios</h1>
            </Route>
          </Switch>


        </Router>
      </ReduxProvider>
    </AuthProvider>

  );
}

export default App
