import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';




const initialState = {
    productos: [],
    producto: {},
    loading: false,
    error: false,
};

const slice = createSlice({
    name: 'producto',
    initialState: initialState,
    reducers: {
        
        //: READ BY ID
        getProductoFech(state) {
            state.loading = true;
            state.error = false;
        },
        getProductoSuccess(state, action) {
            state.producto = action.payload;
            state.loading = false;
            //state.error = false;
        },
        getProductoError(state, action) {
            state.producto = {};
            state.loading = false;
            state.error = true;
        },


        //:: READ ALL 
        getProductosFech(state) {
            state.loading = true;
            state.error = false;
        },
        getProductosSuccess(state, action) {
            state.productos = action.payload;
            state.loading = false;
            state.error = false;
        },
        getProductosError(state, action) {
            state.productos = [];
            state.loading = false;
            state.error = true;
        },



        //:: CREATE
        createProductoFetch(state) {
            state.loading = true;
            state.error = false;
        },
        createProductoSuccess(state, action) {
            const productos = [...state.productos]; //CLON DE PRODUCTOS STATE
                  productos.push(action.payload);   // AGREGAMOS EL NUEVO PRODUCTO AL CLON   
            state.loading = false;
            state.productos = productos;
        },
        createProductoError(state, action) {
            state.loading = false;
            state.error = true;
        },


        //:: UPDATE
        updateProductoFetch(state) {
            state.loading = true;
            state.error = false;
        },
        updateProductoSuccess(state, action) {
            //busco el inidice
            const index = state.productos.findIndex(item => item.id == action.payload.id);
            // reemplazo en un auxiiliar
            const productos = [...state.productos];
                  productos[index] = action.payload;
            //modifico store
            state.loading = false;
            state.productos = productos;
        },
        updateProductoError(state, action) {
            //state.productos = state.productos;
            state.loading = false;
            state.error = true;
        },


        //:: DELETE
        deleteProductoFetch(state) {
            state.loading = true;
            state.error = false;
        },
        deleteProductoSuccess(state, action) {
            //busco el inidice
            const index = state.productos.findIndex(item => item.id == action.payload.id);
            // reemplazo en un auxiiliar
            const productos = [...state.productos];
                  productos.splice(index, 1);
            //modifico store
            state.loading = false;
            state.productos = productos;
        },
        deleteProductoError(state, action) {
            state.loading = false;
            state.error = true;
        }


    }
});



export const getProducto = (id) => async (dispatch) => {
    try {
        debugger;
        dispatch(slice.actions.getProductoFech());
        const { data } = await axios.get(`http://localhost:1337/producto/${id}`);
        debugger;
        dispatch(slice.actions.getProductoSuccess(data))
    } catch (e) {
        console.log(e);
        dispatch(slice.actions.getProductoError())
    }
}

export const getProductos = () => async (dispatch) => {
    try {
        dispatch(slice.actions.getProductosFech());
        const { data } = await axios.get("http://localhost:1337/productos");
        dispatch(slice.actions.getProductosSuccess(data))
    } catch (e) {
        console.log(e);
        dispatch(slice.actions.getProductosError())
    }
}

export const createProducto = (producto) => async (dispatch) => {
    const _producto = {
        ...producto, // segregacion de objetos
        precio: producto.precio + ''
    };
    try {
        dispatch(slice.actions.createProductoFetch());
        const { data } = await axios.post(`http://localhost:1337/productos/${producto.id}`, _producto);
        debugger;
        if (data.id) {
            dispatch(slice.actions.createProductoSuccess(data))
        }
    } catch (e) {
        console.log(e);
        dispatch(slice.actions.createProductoError())
    }
};

export const updateProducto = (producto) => async (dispatch) => {
    const _producto = {
        ...producto, // segregacion de objetos
        precio: producto.precio + ''
    };
    try {
        dispatch(slice.actions.updateProductoFetch());
        const { data } = await axios.put(`http://localhost:1337/productos/${producto.id}`, _producto);
        if (data.id) {
            dispatch(slice.actions.updateProductoSuccess(data))
        }
    } catch (e) {
        console.log(e);
        dispatch(slice.actions.updateProductoError())
    }
};

export const deleteProducto = (id) => async (dispatch) => {
    debugger;
    try {
        dispatch(slice.actions.deleteProductoFetch());
        const { data } = await axios.delete(`http://localhost:1337/productos/${id}`);
        debugger;
        if (data.id) {
            dispatch(slice.actions.deleteProductoSuccess(data))
        }
    } catch (e) {
        debugger
        console.log(e);
        dispatch(slice.actions.deleteProductoError())
    }
}










const productoReducer = slice.reducer;
export { productoReducer }








