import { combineReducers } from '@reduxjs/toolkit';
import { productoReducer } from '../slices/producto';
import { servicioReducer } from '../slices/servicio';

const rootReducer = combineReducers({
    producto:productoReducer,
    servicios:servicioReducer,
});

export default rootReducer;

















